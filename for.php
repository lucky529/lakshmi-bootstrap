<!DOCTYPE html>
<html>
<body>

<?php
$age = array("Peter"=>"35", "Ben"=>"37", "Joe"=>"43");
asort($age);

foreach($age as $x => $x_value) {
  echo "Key=" . $x . ", Value=" . $x_value;
  echo "<br>";
}

$x = 1;
 
while($x <= 5) {
  echo "The number is: $x <br>";
  $x++;
} 

for ($x = 0; $x <= 10; $x++) {
  echo "The number is: $x <br>";
}


?>
